<?php
/**
 * Gallery Plugin
 *
 * PHP version 7
 *
 * @category   Extensions
 * @package    Grav
 * @subpackage lightgallery.js
 * @author     Louison Chevalier
 * @license    http://www.opensource.org/licenses/mit-license.html MIT License
 * @link       https://gitlab.com/louison.chevalier/grav-plugin-gallery
 */
namespace Grav\Plugin;

use Grav\Common\Plugin;

/**
 * Gallery Plugin
 *
 * Class Gallery
 *
 * @category Extensions
 * @package  Grav\Theme
 * @author   Louison CHevaliert>
 * @license  http://www.opensource.org/licenses/mit-license.html MIT License
 * @link    https://gitlab.com/louison.chevalier/grav-plugin-gallery
 */
class GalleryPlugin extends Plugin
{
    /**
     * Register intial event
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0]
        ];
    }

    /**
     * Initialize the plugin and events
     *
     * @return void
     */
    public function onPluginsInitialized()
    {
        if ($this->isAdmin()) {
            $this->enable([
                'onTwigSiteVariables' => ['onTwigSiteVariables', 0],
            ]);
            return;
        }

        if ($this->config->get('plugins.gallery.enabled') != true) {
            return;
        }
        $this->enable(
            [
                'onShortcodeHandlers' => ['shortcodes', 0],
                'onTwigTemplatePaths' => ['templates', 0],
                'onAssetsInitialized' => ['assets', 0]
            ]
        );
    }

    /**
     * Register templates
     *
     * @return void
     */
    public function templates()
    {
        if ($this->config->get('plugins.gallery.templates')) {
            $this->grav['twig']->twig_paths[] = __DIR__ . '/templates';
        }
    }

    /**
     * Register shortcodes
     *
     * @return void
     */
    public function shortcodes()
    {
        if ($this->config->get('plugins.gallery.shortcodes')) {
            $this->grav['shortcode']->registerAllShortcodes(__DIR__ . '/shortcodes');
        }
    }


    /**
     * Set needed variables to display breadcrumbs.
     */
    public function onTwigSiteVariables()
    {
        if ($this->isAdmin()) {
            $this->grav['assets']->add('plugin://gallery/admin/editor-button/js/button.js');
        }
    }


    /**
     * Register assets
     *
     * @return void
     */
    public function assets()
    {
        if ($this->config->get('plugins.gallery.css')) {
            $this->grav['assets']->addCss(
                'plugins://gallery/node_modules/lightgallery/dist/css/lightgallery.css'
            );
        }
        if ($this->config->get('plugins.gallery.js')) {
            $this->grav['assets']->addJs(
                'plugins://gallery/node_modules/lightgallery/dist/js/lightgallery-all.js'
            );
        }
    }
}
